// Lab1_TP.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "Mathlib.h"

using namespace std;
const char EOLN = '\n';
const char YES = 'Y';
const char NO = 'N';
const int LEFT = -1000000000;
const int RIGHT = +1000000000;
const string message = "";
const string continue_message = "Желаете продолжить? Да - Y, Нет - N ";
const string incorrect_message = "Неправильный ввод. Повторите снова> ";
const string input_message = "Введите число> ";
const string out_of_bounds_message = "Число вышло за границы ";
const string skip_characters = "";

//посимвольно отчищаем поток
void ClearInputStream(std::istream& in) 
{
	in.clear();
	while (in.peek() != EOLN && in.peek() != EOF)
	{
		in.get();
	}
}


bool CheckBounds(int a) 
{
	bool ok = (LEFT < a&& a < RIGHT);
	if (!ok)
	{
		cout << out_of_bounds_message << " [" << LEFT << ", " << RIGHT << "]" << endl;
	}
	return ok;
}

//если не конец линии и в конце не пробел
int Seek(std::istream& in) 
{
	while (in.peek() != EOLN && skip_characters.find((char)in.peek()) != string::npos)
	{
		in.get();
	}
	return in.peek();
}

//чекаем на инт
int ReadInt(std::istream& in)
{
	std::cout << input_message;
	int ans;
	in >> ans;
	while (!in || Seek(in) != EOLN || !CheckBounds(ans))//пока не пустой поток или не конец или не вышел за границы 
	{
		ClearInputStream(in);
		std::cout << incorrect_message;
		in >> ans;
	}
	return ans;
}


bool Continue(std::istream& in)
{
	std::cout << continue_message;
	char ans;
	in >> ans;
	while (!in || Seek(in) != EOLN || ans != YES && ans != NO) // пока не пустой поток или пока в конце потока не \n или пока не да и не нет
	{
		ClearInputStream(in);
		std::cout << incorrect_message; 
		in >> ans;
	}
	return ans == YES;//вернуть true если да, false в противном случае
}


int main()
{
	setlocale(LC_ALL, "RUS");
	int a, b, n;
	cout << message << endl;
	bool flag = true;
	while (flag)
	{
		a = ReadInt(cin);
		b = ReadInt(cin);
		n = ReadInt(cin);
		cout << "a = " << a << endl;
		cout << "b = " << b << endl;
		cout << "n = " << n << endl;
		CHfromN(a, b, n);
		flag = Continue(cin);
	}
	return 0;
}

